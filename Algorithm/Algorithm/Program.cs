﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

namespace PrimeAlg
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введiть спосiб вводу данних: \n1)Ввiд з клавiатури\n2)Згенерувати автоматично\n3)Зчитати з файлу");
            int size;

            var timer = new Stopwatch();
            Random rand = new Random();

            //size = 6;

            int[,] OldAarr;
            bool[,] NewAarr;
            List<int> lockedjoin = new List<int>();
            lockedjoin.Add(0);

            try
            {
                int method = Convert.ToInt32(Console.ReadLine());
                Console.Clear();

                Console.WriteLine("Введiть кiлькiсь вершин: ");
                size = Convert.ToInt32(Console.ReadLine());
                OldAarr = new int[size, size];
                NewAarr = new bool[size, size];

                switch (method)
                {
                    case 1:
                        Console.Clear();

                        Console.WriteLine("Введiть числа(через \"ENTER\")");
                        string[] arrstr = new string[size];

                        for (int i = 0; i < size; i++)
                        {
                            arrstr = Console.ReadLine().Split();
                            for (int l = 0; l < size; l++)
                            {
                                OldAarr[i, l] = Convert.ToInt32(arrstr[l]);
                            }
                        }
                        Console.Clear();
                        break;
                    case 2:

                        for (int i = 0; i < size; i++) //generate array
                        {
                            for (int j = i; j < size; j++)
                            {
                                if (rand.Next(2) == 1)
                                {
                                    OldAarr[i, j] = rand.Next(1, 100);
                                    OldAarr[j, i] = OldAarr[i, j];
                                }
                                OldAarr[i, i] = 0;
                            }
                        }

                        break;
                    case 3:
                        Console.WriteLine("Введiть шлях до файлу: ");
                        string path = Console.ReadLine();
                        Console.Clear();

                        using (StreamReader streamReader = new StreamReader(path))
                        {
                            string line;

                            if ((line = streamReader.ReadLine()) == null)
                            {
                                throw new FormatException();
                            }
                            string[] arrstr1 = new string[size];

                            for (int i = 0; i < size; i++)
                            {
                                arrstr = streamReader.ReadLine().Split();
                                for (int l = 0; l < size; l++)
                                {
                                    OldAarr[i, l] = Convert.ToInt32(arrstr[l]);
                                }
                            }
                        }
                        Console.Clear();
                        break;
                    default:
                        throw new FormatException();
                }

                timer.Start();
                for (; ; )
                {
                    int smallestInt = 1000;
                    int newLockedI = 0;
                    int newLockedJ = 0;

                    for (int i = 0; i < lockedjoin.Count; i++)
                    {
                        for (int j = 0; j < size; j++)
                        {
                            if (!lockedjoin.Contains(j))
                                if ((OldAarr[lockedjoin[i], j] < smallestInt) && OldAarr[lockedjoin[i], j] != 0 && !NewAarr[lockedjoin[i], j] && !NewAarr[j, lockedjoin[i]])
                                {
                                    smallestInt = OldAarr[lockedjoin[i], j];
                                    newLockedI = lockedjoin[i];
                                    newLockedJ = j;
                                }
                        }
                    }

                    if (smallestInt != 1000)
                    {
                        NewAarr[newLockedI, newLockedJ] = true;
                        NewAarr[newLockedJ, newLockedI] = true;

                        lockedjoin.Add(newLockedJ);
                    }
                    if (lockedjoin.Count == size)
                        break;
                }
                timer.Stop();

                //for (int i = 0; i < size; i++) //show array
                //{
                //    for (int j = 0; j < size; j++)
                //    {
                //        if (OldAarr[i, j] > 10)
                //            Console.Write(OldAarr[i, j] + " ");
                //        else
                //            Console.Write(OldAarr[i, j] + "  ");
                //        // Console.ReadKey();
                //    }
                //    Console.WriteLine();
                //}
                //Console.WriteLine();
                //for (int i = 0; i < size; i++) //show array
                //{
                //    for (int j = 0; j < size; j++)
                //    {
                //        if (NewAarr[i, j])
                //            Console.Write(1 + " ");
                //        else
                //            Console.Write(0 + " ");
                //        // Console.ReadKey();
                //    }
                //    Console.WriteLine();
                //}

                Console.WriteLine($"Time spent: {timer.ElapsedMilliseconds}");

                Console.ReadKey();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}